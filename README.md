Bamboo Agent APIs
=================
The unofficial cut

This is an public project meant to address the need for remote agent management via a nice rest api.


What it do
----------
*I strongly suggest running with an SDK, or installing on a server where [RAB](https://marketplace.atlassian.com/plugins/com.atlassian.labs.rest-api-browser/server/overview) is present to explore all features*

### Agent State
These operations show and manipulate the agents "Active" state.

- /agent/latest/ (status of all agents)
- /agent/latest/NNNNNN/   (status of agent ID:NNNNNN)
- /agent/latest/NNNNNN/text   (status as shell consumable variable file)
- /agent/latest/NNNNNN/disable   (yep, POST only)
- /agent/latest/NNNNNN/enable   (yep, POST only)

### Security
The intention of many of these APIs is to be called from remote nodes where storing credentials may not be feasible.

Instead any 'sensitive' operations rely on a token that has been granted the proper access.

- /agent/latest/tokens  GET
  returns a listing of all current tokens.
- /agent/latest/tokens  POST
  creates a new token with supplied JSON payload (see samples below)

- /agent/latest/tokens/NNNN GET
  returns details for one token
- /agent/latest/tokens/NNNN PUT
  overrides any details but UUID (token)
- /agent/latest/tokens/NNNN DELETE
  deletes token


Bitbucket Pipelines
--------------------
This project automated ci/cd using bitbucket pipelines.  
Code must be merged into shipit branch to be published to marketplace. See [piplines](pipelines/readme.md) for more information.

1. Test every commit
2. Deploy snapshots of any version merged into `master`
3. Publish release to Atlassian marketplace anything merged into `shipit`

#### Build passes on `shipit`
![Passing Build in Bitbucket](/pipelines/assets/bitbucketpipeline.png "Passing Build in Bitbucket")

#### Version appears in marketplace!
![Published to Marketplace](/pipelines/assets/marketplaceversion.png "Published to Marketplace")





Contributing
------------

Yes please!

Start with a JIRA ticket [here](https://eddiewebb.atlassian.net/browse/AAFB), and submit any coded needed via a Pull Request.

If you want to submit a Pull Request, here's some info on developing Atlassian Plugins

Running the SDK
---------------
You have successfully created an Atlassian Plugin!

Here are the SDK commands you'll use immediately:

* atlas-run   -- installs this plugin into the product and starts it on localhost
* atlas-debug -- same as atlas-run, but allows a debugger to attach at port 5005
* atlas-cli   -- after atlas-run or atlas-debug, opens a Maven command line window:
                 - 'pi' reinstalls the plugin into the running product instance
* atlas-help  -- prints description for all commands in the SDK

Full documentation is always available at:

https://developer.atlassian.com/display/DOCS/Introduction+to+the+Atlassian+Plugin+SDK
