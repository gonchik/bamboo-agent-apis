package it.com.edwardawebb.bamboo.agentapis.rest;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.ws.rs.core.MediaType;

import org.apache.wink.client.ClientConfig;
import org.apache.wink.client.ClientResponse;
import org.apache.wink.client.EntityType;
import org.apache.wink.client.RestClient;
import org.apache.wink.client.handlers.BasicAuthSecurityHandler;
import org.codehaus.jackson.jaxrs.JacksonJaxbJsonProvider;
import org.junit.After;
import org.junit.Test;

import com.edwardawebb.bamboo.agentapis.rest.admin.TokenResource;

public class TokenEndpointTest extends AbstractEndpointTest{

    final static String AGENT_ID="131074";
    final static String FAKE_UUID ="7ba167aa-631c-4771-b197-fcb459cc7fd7";
    
    
    
    @After
    public void tearDown() {

    }
    

    
    @Test
    //NOTE: the opposite of these commands are tested thouroughly below using admin accounts.
    public void nonAdminsCantTouchTokens(){
        javax.ws.rs.core.Application app = new javax.ws.rs.core.Application() {
            public Set<Class<?>> getClasses() {
                Set<Class<?>> classes = new HashSet<Class<?>>();
                classes.add(JacksonJaxbJsonProvider.class);
                return classes;
            }

        };
        ClientConfig clientConfigUser = new ClientConfig();
        clientConfigUser.applications(app);
        BasicAuthSecurityHandler basicAuthSecurityHandler = new BasicAuthSecurityHandler();
        basicAuthSecurityHandler.setUserName("user");
        basicAuthSecurityHandler.setPassword("user"); 
        clientConfigUser.handlers(basicAuthSecurityHandler);
        //create client usin auth   
        RestClient clientUser = new RestClient(clientConfigUser);
        //validate user account exists
        ClientResponse response = clientUser.resource(resourceUrlToken).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).get();
        assertNotSame("No user account found, tests expect a user:user account with no admin rights",401,response.getStatusCode());
        
        //list
        assertEquals("Did not return forbidden code",403,response.getStatusCode());
        
        //create        
        response = clientUser.resource(resourceUrlToken).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).post(readOnlyToken);
        assertEquals("Did not return forbidden code",403,response.getStatusCode());
        
       
        //update
        response = clientUser.resource(resourceUrlToken + "/" + FAKE_UUID).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).put(readOnlyToken);
        assertEquals("Did not return forbidden code",403,response.getStatusCode());
        
        
        //delete
        response = clientUser.resource(resourceUrlToken + "/" + FAKE_UUID).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).delete();
        assertEquals("Did not return forbidden code",403,response.getStatusCode());
        
    }
    
    
    @Test
    public void aReadOnlyTokenCanBeCreatedAsRequested(){
        ClientResponse response = client.resource(resourceUrlToken).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).post(readOnlyToken);
        assertEquals("Could not create token needed for test",200,response.getStatusCode());
        readOnlyToken = response.getEntity(TokenResource.class);
        assertNotNull("Returned token does not have UUID",readOnlyToken.getUuid());
        assertEquals("Returned token permission for read not what expected",true,readOnlyToken.isAllowedRead());
        assertEquals("Returned token premission for write not what expected",false,readOnlyToken.isAllowedChange() );
    
    }


    @Test
    public void aReadWriteTokenCanBeCreatedAsRequested(){ 
        ClientResponse response = client.resource(resourceUrlToken).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).post(readAndWriteToken);
        assertEquals("Could not create token needed for test",200,response.getStatusCode());
        readAndWriteToken = response.getEntity(TokenResource.class);
        assertNotNull("Returned token does not have UUID",readAndWriteToken.getUuid());
        assertEquals("Returned token permission for read not what expected",true,readAndWriteToken.isAllowedRead());
        assertEquals("Returned token permission for write expected",true,readAndWriteToken.isAllowedChange() );
    
    }

    @Test
    public void aTokenCanBeUpdated(){ 
        //given a new token
        ClientResponse response = client.resource(resourceUrlToken).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).post(readAndWriteToken);
        assertEquals("Could not create token needed for test",200,response.getStatusCode());
        readAndWriteToken = response.getEntity(TokenResource.class);
        assertNotNull("Returned token does not have UUID",readAndWriteToken.getUuid());
        assertEquals("Returned token permission for read not what expected",true,readAndWriteToken.isAllowedRead());
        assertEquals("Returned token permission for write expected",true,readAndWriteToken.isAllowedChange() );
        
        //when we send updates
        TokenResource doNOthingToken = new TokenResource(readAndWriteToken.getUuid(),"NOt permissions", false, false);
        response = client.resource(resourceUrlToken + "/" + readAndWriteToken.getUuid()).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).put(doNOthingToken);
        assertEquals("Could not update token needed for test",200,response.getStatusCode());
        
        //they are persisted
         response = client.resource(resourceUrlToken + "/" + readAndWriteToken.getUuid()).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).get();
        assertEquals("Could not retrieve token needed for test",200,response.getStatusCode());
        TokenResource updatedToken = response.getEntity(TokenResource.class);
        assertNotNull("Returned token does not have UUID",updatedToken.getUuid());
        assertEquals("Returned token permission for read not what expected",false,updatedToken.isAllowedRead());
        assertEquals("Returned token permission for write expected",false,updatedToken.isAllowedChange() );
    }
    
    @Test
    public void aListOfTokenCanBeRetrieved(){ 
        //given a list of token
        ClientResponse response = client.resource(resourceUrlToken).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).get();
        assertEquals("Could not create token needed for test",200,response.getStatusCode());
        List<TokenResource> allTokens = (List<TokenResource>) response.getEntity(new EntityType<List<TokenResource>>() {});
        assertThat(allTokens.size(),org.hamcrest.CoreMatchers.not(0) );
        
        for (Iterator iterator = allTokens.iterator(); iterator.hasNext();) {
            TokenResource tokenResource = (TokenResource) iterator.next();
            //when we send DLEETE
            response = client.resource(resourceUrlToken + "/" + tokenResource.getUuid()).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).delete();
            assertEquals("Could not update delete needed for test",200,response.getStatusCode());
            
            //subsequents requests get 404
             response = client.resource(resourceUrlToken + "/" + tokenResource.getUuid()).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).get();
            assertEquals("Could not retrieve token needed for test",404,response.getStatusCode());
        }
       
    }
    
    @Test
    public void aTokenCanBeDeleted(){ 
        //given a new token
        ClientResponse response = client.resource(resourceUrlToken).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).post(readAndWriteToken);
        assertEquals("Could not create token needed for test",200,response.getStatusCode());
        readAndWriteToken = response.getEntity(TokenResource.class);
        assertNotNull("Returned token does not have UUID",readAndWriteToken.getUuid());
        
        //when we send DLEETE
        response = client.resource(resourceUrlToken + "/" + readAndWriteToken.getUuid()).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).delete();
        assertEquals("Could not update delete needed for test",200,response.getStatusCode());
        
        //subsequents requests get 404
         response = client.resource(resourceUrlToken + "/" + readAndWriteToken.getUuid()).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).get();
        assertEquals("Could not retrieve token needed for test",404,response.getStatusCode());
    }
    
    
    
}
