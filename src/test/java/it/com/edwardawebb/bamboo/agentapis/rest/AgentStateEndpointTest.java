package it.com.edwardawebb.bamboo.agentapis.rest;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;

import javax.ws.rs.core.MediaType;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.wink.client.ClientResponse;
import org.apache.wink.client.EntityType;
import org.hamcrest.Matcher;
import org.junit.After;
import org.junit.Test;

import com.edwardawebb.bamboo.agentapis.rest.admin.TokenResource;
import com.edwardawebb.bamboo.agentapis.rest.agents.state.AgentStateModel;

import java.util.List;

public class AgentStateEndpointTest extends AbstractEndpointTest{

    final static String AGENT_ID="131073";
    final static String AGENT_ID_TWO="131074";



    @After
    public void tearDown() {
    }

    @Test
    public void allAgentStateCanBeRetrievedWithValidToken() {
        //enable both agents
        resetReadAndWriteToken();
        ClientResponse response = client.resource(String.format(resourceUrlAgentState,  AGENT_ID)+ "/enable?uuid=" + readAndWriteToken.getUuid()).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).post(true);
        assertEquals("Could not enable agent",200,response.getStatusCode());
        response = client.resource(String.format(resourceUrlAgentState,  AGENT_ID_TWO)+ "/enable?uuid=" + readAndWriteToken.getUuid()).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).post(true);
        assertEquals("Could not enable agent",200,response.getStatusCode());


        //get status URL ready with read only token UUID
        resetReadOnlyToken();
        String statusUrl = agentBaseUrl +"?uuid=" + readOnlyToken.getUuid();

        //turn response into list
        response = client.resource(statusUrl)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .get();
        assertEquals("Unexpected Error Code",200,response.getStatusCode());
        List<AgentStateModel> agentStateList = response.getEntity(new EntityType<List<AgentStateModel>>() {});

        //expect two agents with matching IDs
        assertEquals("Agent list count wrong", 2L, agentStateList.size());
        Matcher<AgentStateModel> agentOne = allOf(hasProperty("id", is(Long.parseLong(AGENT_ID))), hasProperty("enabled", is(true)), hasProperty("busy", is(false)));
        Matcher<AgentStateModel> agentTwo = allOf(hasProperty("id", is(Long.parseLong(AGENT_ID_TWO))), hasProperty("enabled", is(true)), hasProperty("busy", is(false)));
        for(AgentStateModel model : agentStateList){
            System.out.println(ReflectionToStringBuilder.toString(model));
        }
        assertThat( agentStateList, containsInAnyOrder(
                agentOne, agentTwo
        ));
    }

    @Test
    public void agentStatusCanBeRetrievedWithValidToken() {
        resetReadOnlyToken();
        ClientResponse response = client.resource(String.format(resourceUrlAgentState,  AGENT_ID) +"?uuid=" + readOnlyToken.getUuid()).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).get();
        assertEquals("Unexpected Error Code",200,response.getStatusCode());
        AgentStateModel agentState = response.getEntity(AgentStateModel.class);
        assertEquals("Retrieved invalid agent ID",Long.parseLong(AGENT_ID),agentState.getId());
    }
    @Test
    public void agentStatusIncludesOnlineStatus() {
        resetReadOnlyToken();
        ClientResponse response = client.resource(String.format(resourceUrlAgentState,  AGENT_ID) +"?uuid=" + readOnlyToken.getUuid()).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).get();
        assertEquals("Unexpected Error Code",200,response.getStatusCode());
        AgentStateModel agentState = response.getEntity(AgentStateModel.class);
        assertEquals("Retrieved invalid agent ID",Long.parseLong(AGENT_ID),agentState.getId());
        assertEquals("Online status not reported",true,agentState.isOnline());
    }

    @Test
    public void anAgentCanNotBeDisabledWithReadOnlyToken(){
        resetReadOnlyToken();
        ClientResponse response = client.resource(String.format(resourceUrlAgentState,  AGENT_ID)+ "/disable?uuid=" + readOnlyToken.getUuid()).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).post(true);
        assertEquals("Was allowed to change with read only token",403,response.getStatusCode());
    }
    
    @Test
    public void anAgentCanBeDisabledWithWriteToken(){
        resetReadAndWriteToken();
        ClientResponse response = client.resource(String.format(resourceUrlAgentState,  AGENT_ID)+ "/disable?uuid=" + readAndWriteToken.getUuid()).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).post(true);
        assertEquals("Could not disable agent",200,response.getStatusCode());
    }

    @Test
    public void anAgentCanNotBeEnabledWithReadOnlyToken(){
        resetReadOnlyToken();
        ClientResponse response = client.resource(String.format(resourceUrlAgentState,  AGENT_ID)+ "/enable?uuid=" + readOnlyToken.getUuid()).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).post(true);
        assertEquals("Was allowed to change with read only token",403,response.getStatusCode());
    }
    
    @Test
    public void anAgentCanBeEnabledWithWriteToken(){
        resetReadAndWriteToken();
        ClientResponse response = client.resource(String.format(resourceUrlAgentState,  AGENT_ID)+ "/enable?uuid=" + readAndWriteToken.getUuid()).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).post(true);
        assertEquals("Could not enable agent",200,response.getStatusCode());
    }
}
