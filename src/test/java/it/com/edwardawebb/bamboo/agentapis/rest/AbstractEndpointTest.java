package it.com.edwardawebb.bamboo.agentapis.rest;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.core.MediaType;

import org.apache.wink.client.ClientConfig;
import org.apache.wink.client.ClientResponse;
import org.apache.wink.client.Resource;
import org.apache.wink.client.RestClient;
import org.apache.wink.client.handlers.BasicAuthSecurityHandler;
import org.codehaus.jackson.jaxrs.JacksonJaxbJsonProvider;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;
import org.junit.Before;

import com.edwardawebb.bamboo.agentapis.rest.admin.TokenResource;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class AbstractEndpointTest {
    private static final String USERNAME="admin";
    private final static String PASSWORD="admin";
    
    protected ClientConfig clientConfig;
    protected  RestClient client;
    String agentBaseUrl = System.getProperty("baseurl") + "/rest/agents/1.0/";
    String configBaseUrl = System.getProperty("baseurl") + "/rest/agent-config/1.0/";

    String resourceUrlToken = configBaseUrl + "tokens";
    String resourceUrlAgentState = agentBaseUrl + "%s/state";
    
     TokenResource readOnlyToken = new TokenResource("readOnly", true, false);
     TokenResource readAndWriteToken = new TokenResource("readOnly", true, true);

    @Before
    public void setup(){

        javax.ws.rs.core.Application app = new javax.ws.rs.core.Application() {
            public Set<Class<?>> getClasses() {
                Set<Class<?>> classes = new HashSet<Class<?>>();
                classes.add(JacksonJaxbJsonProvider.class);
                return classes;
            }

        };
        //create auth handler
        clientConfig = new ClientConfig();
        clientConfig.applications(app);
        BasicAuthSecurityHandler basicAuthSecurityHandler = new BasicAuthSecurityHandler();
        basicAuthSecurityHandler.setUserName(USERNAME);
        basicAuthSecurityHandler.setPassword(PASSWORD); 
        clientConfig.handlers(basicAuthSecurityHandler);
        //create client usin auth   
        client = new RestClient(clientConfig);
      
    }



    public void resetReadOnlyToken() {
        ClientResponse response = client.resource(resourceUrlToken).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).post(readOnlyToken);
        assertEquals("Could not create token needed for test",200,response.getStatusCode());
        this.readOnlyToken = response.getEntity(TokenResource.class);
        assertFalse("Token should not have write access, but does", readOnlyToken.isAllowedChange());
    }
    public void resetReadAndWriteToken() {
        ClientResponse response = client.resource(resourceUrlToken).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).post(readAndWriteToken);
        assertEquals("Could not create token needed for test",200,response.getStatusCode());
        this.readAndWriteToken=response.getEntity(TokenResource.class);
        assertTrue("TOken does not have write access", readAndWriteToken.isAllowedChange());
    }
}
