package it.com.edwardawebb.bamboo.agentapis.rest;

import static org.junit.Assert.assertEquals;

import javax.ws.rs.core.MediaType;

import org.apache.wink.client.ClientResponse;
import org.junit.After;
import org.junit.Test;

import com.edwardawebb.bamboo.agentapis.enums.PermissionEnum;
import com.edwardawebb.bamboo.agentapis.rest.admin.TokenResource;

public class ChaperoneEndpointTest extends AbstractEndpointTest {

    final static String AGENT_ID = "131073";
    final static String AGENT_ID_TWO = "131074";
    private final String upgradeUrlFormat = agentBaseUrl + "%s/maintenance?uuid=%s";
    private final String finishUrlFormat = agentBaseUrl + "123/maintenance/%s/finish?uuid=%s";
    private final String configUrl = configBaseUrl + "admin-config";

    private static final String VERSION = "1.2.3";

    @After
    public void tearDown() {

    }

    @Test
    public void whenSTableSTateNoAgentsAreGivenPermission() {
        // set config to stable
        ClientResponse response = client.resource(configUrl).contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON).put("{\"stance\":\"STABLE\",\"maxactive\":\"2\"}");
        assertEquals("Could not set permission to STABLE state.", 204, response.getStatusCode());

        response = client.resource(resourceUrlToken).contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON).post(readAndWriteToken);
        assertEquals("Could not create token needed for test", 200, response.getStatusCode());
        readAndWriteToken = response.getEntity(TokenResource.class);
        response = client.resource(getUpgradeUrl(AGENT_ID)).contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.WILDCARD).post(null);
        assertEquals("Could not retrive status", 200, response.getStatusCode());
        String permission = response.getEntity(String.class);
        assertEquals("System is allowing agents when it shoudl not be", PermissionEnum.NO_CHILD.name(),
                getValuePermissionFrom(permission));
    }

    @Test
    public void whenLatestStatePermissionIsGiven() {

        // set config to stable
        ClientResponse response = client.resource(configUrl).contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON).put("{\"stance\":\"LATEST\",\"maxactive\":\"2\"}");
        assertEquals("Could not set permission to LATEST state.", 204, response.getStatusCode());

        response = client.resource(resourceUrlToken).contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON).post(readAndWriteToken);
        assertEquals("Could not create token needed for test", 200, response.getStatusCode());
        readAndWriteToken = response.getEntity(TokenResource.class);
        response = client.resource(getUpgradeUrl(AGENT_ID)).contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.WILDCARD).post(null);
        assertEquals("Could not retrive status", 200, response.getStatusCode());
        String permission = response.getEntity(String.class);
        assertEquals("Retrieved invalid permission", PermissionEnum.YES_CHILD.name(),
                getValuePermissionFrom(permission));
        assertEquals("Retrieved invalid version", "LATEST", getValueVersion(permission));
        // mark complete
        response = client.resource(getFinishUrl(permission)).contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.WILDCARD).put(null);String text = response.getEntity(String.class);
              //  assertEquals("Could not mark task finished, and agent active", "true", getValueEnabled(text));

    }

    @Test
    public void whenTargetStatePermissionIsGiven() {
        // set config to stable
        ClientResponse response = client.resource(configUrl).contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .put("{\"stance\":\"TARGET\",\"maxactive\":\"2\",\"targetVersion\":\"" + VERSION + "\"}");
        assertEquals("Could not set permission to LATEST state.", 204, response.getStatusCode());

        response = client.resource(resourceUrlToken).contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON).post(readAndWriteToken);
        assertEquals("Could not create token needed for test", 200, response.getStatusCode());
        readAndWriteToken = response.getEntity(TokenResource.class);
        response = client.resource(getUpgradeUrl(AGENT_ID)).contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.WILDCARD).post(null);
        assertEquals("Could not retrive status", 200, response.getStatusCode());
        String permission = response.getEntity(String.class);
        assertEquals("Retrieved invalid permission", PermissionEnum.YES_CHILD.name(),
                getValuePermissionFrom(permission));
        // mark complete
        response = client.resource(getFinishUrl(permission)).contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.WILDCARD).put(null);
        String text = response.getEntity(String.class);
        //assertEquals("Could not mark task finished, and agent active", "true", getValueEnabled(text));
    }

    @Test
    public void whenTargetStateTheGivenVersionIsCorrect() {
        // set config to stable
        ClientResponse response = client.resource(configUrl).contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .put("{\"stance\":\"TARGET\",\"maxactive\":\"2\",\"targetVersion\":\"" + VERSION + "\"}");
        assertEquals("Could not set permission to LATEST state.", 204, response.getStatusCode());

        response = client.resource(resourceUrlToken).contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON).post(readAndWriteToken);
        assertEquals("Could not create token needed for test", 200, response.getStatusCode());
        readAndWriteToken = response.getEntity(TokenResource.class);

        response = client.resource(getUpgradeUrl(AGENT_ID)).contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.WILDCARD).post(null);
        assertEquals("Could not retrive status", 200, response.getStatusCode());
        String permission = response.getEntity(String.class);
        assertEquals("Retrieved invalid version", VERSION, getValueVersion(permission));
        // mark complete
        response = client.resource(getFinishUrl(permission)).contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.WILDCARD).put(null);
        String text = response.getEntity(String.class);
        //assertEquals("Could not mark task finished, and agent active", "true", getValueEnabled(text));
    }

    @Test
    public void agentsPastLimitAreNotAllowed() {
        // set config to stable
        ClientResponse response = client.resource(configUrl).contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .put("{\"stance\":\"TARGET\",\"maxactive\":\"1\",\"targetVersion\":\"" + VERSION + "\"}");
        assertEquals("Could not set permission to LATEST state.", 204, response.getStatusCode());

        response = client.resource(resourceUrlToken).contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON).post(readAndWriteToken);
        assertEquals("Could not create token needed for test", 200, response.getStatusCode());
        readAndWriteToken = response.getEntity(TokenResource.class);
        // first agent allowed
        response = client.resource(getUpgradeUrl(AGENT_ID)).contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.WILDCARD).post(null);
        assertEquals("Could not retrive status", 200, response.getStatusCode());
        String permission = response.getEntity(String.class);
        assertEquals("Retrieved invalid permission", PermissionEnum.YES_CHILD.name(),
                getValuePermissionFrom(permission));
        // second is not
        response = client.resource(getUpgradeUrl(AGENT_ID)).contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.WILDCARD).post(null);
        assertEquals("Could not retrive status", 200, response.getStatusCode());
        String permissionTwo = response.getEntity(String.class);
        assertEquals("Retrieved invalid permission", PermissionEnum.YES_CHILD.name(),
                getValuePermissionFrom(permission));
        // mark 1st complete
        client.resource(getFinishUrl(permission)).contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.WILDCARD).put(null);
        // second is now allowed
        response = client.resource(getUpgradeUrl(AGENT_ID)).contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.WILDCARD).post(null);
        assertEquals("Could not retrive status", 200, response.getStatusCode());
        permissionTwo = response.getEntity(String.class);
        assertEquals("Retrieved invalid permission", PermissionEnum.YES_CHILD.name(),
                getValuePermissionFrom(permissionTwo));
        // mark 2nd complete
        response = client.resource(getFinishUrl(permissionTwo)).contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.WILDCARD).put(null);
        String text = response.getEntity(String.class);
       // assertEquals("Could not mark task finished, and agent active", "true", getValueEnabled(text));
    }

    @Test
    public void agentsCanBeMarkedCompleteAndActive() {

        // set config to stable
        ClientResponse response = client.resource(configUrl).contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .put("{\"stance\":\"TARGET\",\"maxactive\":\"2\",\"targetVersion\":\"" + VERSION + "\"}");
        assertEquals("Could not set permission to LATEST state.", 204, response.getStatusCode());

        response = client.resource(resourceUrlToken).contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON).post(readAndWriteToken);
        assertEquals("Could not create token needed for test", 200, response.getStatusCode());
        readAndWriteToken = response.getEntity(TokenResource.class);
        response = client.resource(getUpgradeUrl(AGENT_ID_TWO)).contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.WILDCARD).post(null);
        assertEquals("Could not retrive status", 200, response.getStatusCode());
        String permission = response.getEntity(String.class);
        assertEquals("Retrieved invalid permission", PermissionEnum.YES_CHILD.name(),
                getValuePermissionFrom(permission));
        // mark complete
        response = client.resource(getFinishUrl(permission))
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.WILDCARD).put(null);
        String text = response.getEntity(String.class);
        assertEquals("Could not mark task finished, and agent active", "true", getValueEnabled(text));
    }

    private Object getValueEnabled(String permission) {
        int codeStart = permission.indexOf("ENABLED=")+8;
        int codeEnd = permission.indexOf("BUSY=")-1;
        System.out.println("Test: returning ENabled state: " + permission.substring(codeStart, codeEnd));
        return permission.substring(codeStart, codeEnd);
    }

    private Object getValuePermissionFrom(String permission) {
        int codeStart = permission.indexOf("PCODE=");
        int codeEnd = permission.indexOf("PMESSAGE=")-1;
        System.out.println("Test: returning Code"+permission.substring(codeStart + 6, codeEnd));
        return permission.substring(codeStart + 6, codeEnd);
    }

    private Object getValueVersion(String permission) {
        int codeStart = permission.indexOf("PVERSION=");
        int codeEnd = permission.indexOf("ENABLED=")-1;
        System.out.println("Test: Returning Version:" + permission.substring(codeStart + 9, codeEnd));
        return permission.substring(codeStart + 9, codeEnd);
    }

    private Object getValueTaskId(String permission) {
        int codeStart = permission.indexOf("TASK=");
        int codeEnd = permission.length()-1;
        System.out.println("Test: Returning Task ID:" + permission.substring(codeStart + 5, codeEnd));
        return permission.substring(codeStart + 5, codeEnd);
    }
    
    private String getUpgradeUrl(String agentId){
        return String.format(upgradeUrlFormat,agentId,readAndWriteToken.getUuid());
    }
    
    private String getFinishUrl(String permission){
        
        return String.format(finishUrlFormat,getValueTaskId(permission),readAndWriteToken.getUuid());
        
    }

}
