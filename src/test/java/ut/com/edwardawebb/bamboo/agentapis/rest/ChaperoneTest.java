package ut.com.edwardawebb.bamboo.agentapis.rest;

import com.edwardawebb.bamboo.agentapis.rest.admin.AdminConfigurationResource;
import com.edwardawebb.bamboo.agentapis.rest.agents.maintenance.VersionModel;
import com.edwardawebb.bamboo.agentapis.services.ChaperoneService;
import com.edwardawebb.bamboo.agentapis.services.ConfigurationService;
import com.edwardawebb.bamboo.agentapis.services.DefaultChaperoneService;
import com.edwardawebb.bamboo.agentapis.services.DefaultConfigurationService;
import org.junit.Test;
import org.junit.After;
import org.junit.Before;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.*;


public class ChaperoneTest {
    ChaperoneService chaperoneService;


    @Before
    public void setup() {

    }

    @After
    public void tearDown() {

    }

    @Test
    public void versionCanBePolled() {
        String expectedVersion = "1.2.3";
        AdminConfigurationResource config = new AdminConfigurationResource();
        config.setTargetVersion(expectedVersion);
        ConfigurationService configurationService = mock(DefaultConfigurationService.class);
        when(configurationService.getActiveConfig()).thenReturn(config);
        chaperoneService = new DefaultChaperoneService(null,null,configurationService);
        VersionModel version = chaperoneService.pollVersion();
        assertThat(version.targetVersion, is(expectedVersion));
    }
}
