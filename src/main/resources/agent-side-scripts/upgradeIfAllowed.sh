#!/bin/bash
#
#  This script should be triggered by crontab or similar scheduler on regular interval (i.e, Daily)
#   It will check in with the master server, and execute "UPGRADE_JOB" if allowed.
#



#
#   CHANGE THIS STUFF
#
CORE_DOMAIN="localhost:6990/bamboo" #domain, port and context without environment prefixes.
## change case statement on line 41 if your prefixes are not dev-,test-,perf- or you don't use https.

UUID="7ba167aa-631c-4771-b197-fcb459cc7fd7"

SIBLING_PATIENCE_TIME=2 # will wait 10 seconds
SIBLING_PATIENCE_COUNT=9 # will repeat 9 times.




#
# Edit this to do whatever is considered a local upgrade.
# consider lifecycle, its very likely this running shell will be terminated a part of upgrade.
# SOMETHING will need to tell the master server everything is OK once back online
#  /rest/agent/latest/mothermayi/finish/<task_ID>.text
#
upgradeLocalAgent(){

    # save taskID to a file we can use after restart
    cp $TMPFILE/state.txt ~/finishTaskOnStartup.txt


    # call commands, chef, puppet, etc here.
    echo "beep bop, upgrading and saving task ID to file."



}




























#
#  DON'T CHANGE THIS STUFF
#  (unless you know why :) )
#

echo "Running Upgrade Check for Agent $agentId"



#harvest host name to know environment, and agent ID from config.
#agentId=`cat /opt/lforge/atlassian_data/bamboo_home/bamboo-agent.cfg.xml | grep -oPm1 "(?<=<id>)[^<]+"`
agentId=131073

thisbox=`hostname`
envKey=${thisbox:4:1}
case "$envKey" in
    d)
    bambooUrl=https://dev-${CORE_DOMAIN}
    ;;
    t)
    bambooUrl=https://test-${CORE_DOMAIN}
    ;;
    f)
    bambooUrl=https://perf-${CORE_DOMAIN}
    ;;
    p)
    bambooUrl=https://${CORE_DOMAIN}
    ;;
    e)
#local testing
    bambooUrl=http://${CORE_DOMAIN}
    ;;
    *)
    echo "Unable to decipher environment from host $thisbox"
    exit 2
    ;;
esac




let attempts=1
checkBambooMaster(){
    curl -H "uuid: ${UUID}" -X POST -k -b $TMPFILE/cookies "$bambooUrl/rest/agent/latest/mothermayi/upgrade/$agentId.text" -o $TMPFILE/state.txt 2>/dev/null


    # MOnitor status until the agent is idle
    source $TMPFILE/state.txt
    if [ "$PCODE" == "YES_CHILD" ]
    then
        #server says we can upgrade, make sure we are idle.
        echo "Master server says I can upgrade to version ${PVERSION}"
        if [ "$BUSY"  == "true" ]; then
            printf "\tAgent is still running a job, waiting ..\n" 
            # while polling, and is still running, slee
            running=1
            while [ $running -eq 1 ]
            do
                sleep 60
                curl -k -b $TMPFILE/cookies "$bambooUrl/rest/agent/latest/$agentId/text" -o $TMPFILE/state.txt 2>/dev/null
                source $TMPFILE/state.txt

                if [ "$BUSY"  == "false" ]; then
                    printf "\tYay, agent is now idle!\n"
                    break
                else
                    printf "\tstill busy..\n"
                fi
            done
        fi
    elif [ "$PCODE" == "WAIT_FOR_SIBLINGS" ]
    then
        # allowed to upgrae, but too many others are working right now, check back in a few

        echo "Master server wants me to wait, this is my $attempts attempt."
        echo "${PCODE}: ${PMESSAGE}"
        if [ $attempts -gt $SIBLING_PATIENCE_COUNT ]
        then
            echo "Siblings have exhausted my patience. INcrease wait times, offset cycles, or increase concurrency"
            exit 9
        fi
        let attempts+=1
        sleep $SIBLING_PATIENCE_TIME
        checkBambooMaster #will recurse back into this functuion
    elif [ "$PCODE" == "NO_CHILD" ]
    then
        echo "Master server says I can not upgrade now."
        echo "$PCODE: $PMESSAGE"
        exit 0
    elif [ "$PCODE" == "UH_OH" ]
    then
        echo "ERROR:  Master server is reporting an issue."
        echo "$PCODE: $PMESSAGE ,  existing Task ID: $TASK"
        exit 0
    else

        echo "ERROR:  I don't understand server response!"
        cat $TMPFILE/state.txt
        exit 9
    fi

}









#check for required libraries/tools and setup tempdir
command -v curl >/dev/null 2>&1 || { echo "Required tool 'curl' not found" ;exit 2; }
scriptname=`basename $0`
TMPFILE=`mktemp -d /tmp/requestUpgrade.XXXXXX` || exit 1





# disable agent in bamboo
echo 
echo "Checking upgrade rules on master server ${bambooUrl}"
curl -k -c $TMPFILE/cookies "$bambooUrl" > /dev/null 2>&1


checkBambooMaster



# run clear disk commands for build-dir older then 30 days.
echo
echo "Agent is disabled and idle, starting upgrade"


upgradeLocalAgent


