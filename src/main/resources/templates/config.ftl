<html>
  <head>
  <meta name="decorator" content="atl.admin">
  ${webResourceManager.requireResource("com.atlassian.auiplugin:ajs")}
  ${webResourceManager.requireResource("com.atlassian.auiplugin:aui-table-sortable")}
  ${webResourceManager.requireResource("com.edwardawebb.bamboo-agent-apis:admin-resources")}
    <title>${i18n.getText("agent-apis.admin.title")}</title>
  </head>
  <body>
  [@ui.header pageKey="agent-apis.admin.heading" /]
<button class="aui-button aui-button-subtle" href="#" id="popupLink">
    <span class="aui-icon aui-icon-small aui-iconfont-help">help</span> What is this page?
    <div id="popupHelpBody" style="display:none;">${i18n.getText("agent-apis.admin.page.info.popup")}</div>
</button>
  
    <div id="aui-message-bar"></div>
    <form id="agentApiConfigForm" class="aui">
    
       <fieldset>
	      [@ww.select name="stance" descriptionKey="agent-apis.admin.stance.description" labelKey="agent-apis.admin.stance" cssClass="aui" required='true' list="upgradeStanceValues"/]
	      
       		 [@ui.bambooSection dependsOn='stance' showOn='TARGET']	              
		       [@ww.textfield  descriptionKey="agent-apis.admin.targetVersion.description"  labelKey="agent-apis.admin.targetVersion" name="targetVersion" required='true'/]	   
	        [/@ui.bambooSection]
	        
	        [@ui.bambooSection dependsOn='stance' showOn='TARGET LATEST']
	        
        <div class="field-group" style="width:500px">
	        <div class="aui-message aui-message-hint description">
			    <p class="title">
			        <strong>TARGET & LATEST</strong>
			    </p>
			    <p> ${i18n.getText("agent-apis.admin.statedescription.targetAndLatest")}</p>
			</div>
			</div>
		        [@ww.textfield  descriptionKey="agent-apis.admin.maxactive.description"  labelKey="agent-apis.admin.maxactive" name="maxactive" required='true'/]
	            
	       [/@ui.bambooSection]
	       
	        [@ui.bambooSection dependsOn='stance' showOn='STABLE']	
        <div class="field-group" style="width:500px">
		        <div class="aui-message description">
				    <p class="title">
				        <strong>Steady/Stable State</strong>
				    </p>
				    <p>${i18n.getText("agent-apis.admin.statedescription.stable")}</p>
				</div>
				</div>
	        [/@ui.bambooSection]
		    
      </fieldset>
      
      <div>
        <input type="submit" value="Save" class="aui-button aui-button-primary">
      </div>
    </form>



    <h4>Outstanding Tasks</h4>
    <p>All open tasks will be listed below. You should only delete these
    if the agent failed, and was manually resolved, and back online, or
    permanently offline.</p>

    <!-- list existing -->
    <div class="tasklist">
    	<table class="aui aui-table-sortable" width="90%">
            <thead>
                <tr>
                    <th>Task ID</th>
                    <th>Agent</th>
                    <th>Token UUID</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody id="taskRows">

            </tbody>
        </table>
    </div>
    
    
  </body>
</html>