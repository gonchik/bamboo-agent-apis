package com.edwardawebb.bamboo.agentapis;

public class AgentNotValidException extends RuntimeException {

    public AgentNotValidException(String string) {
        super(string);
    }

}
