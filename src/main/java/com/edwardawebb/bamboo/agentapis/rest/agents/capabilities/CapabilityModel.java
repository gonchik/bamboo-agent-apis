package com.edwardawebb.bamboo.agentapis.rest.agents.capabilities;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.atlassian.bamboo.v2.build.agent.capability.CapabilitySet;
import com.atlassian.bamboo.v2.build.agent.capability.ReadOnlyCapabilitySet;



@XmlRootElement(name = "capability")
@XmlAccessorType(XmlAccessType.FIELD)
public class CapabilityModel {

    private String key;
    private String description;
    private String value;
    
    public String getKey() {
        return key;
    }
    public void setKey(String key) {
        this.key = key;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getValue() {
        return value;
    }
    public void setValue(String value) {
        this.value = value;
    }
    
    public static List<CapabilityModel> fromCapabilitySet(ReadOnlyCapabilitySet capabilities){
        List<CapabilityModel> result = new ArrayList<CapabilityModel>();
        for (com.atlassian.bamboo.v2.build.agent.capability.Capability capability : capabilities.getCapabilities()) {

            CapabilityModel model = new CapabilityModel();
            model.setKey(capability.getKey());
            model.setValue(capability.getValue());
            result.add(model);
        }
        return result;
        
    }
    
}
