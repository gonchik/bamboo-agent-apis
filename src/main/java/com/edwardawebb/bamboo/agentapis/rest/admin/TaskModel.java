package com.edwardawebb.bamboo.agentapis.rest.admin;

import com.edwardawebb.bamboo.agentapis.enums.AgentActivity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

/**
 *
 * 
 */
@XmlRootElement(name = "task")
@XmlAccessorType(XmlAccessType.FIELD)
public class TaskModel {

    @XmlElement(name = "id")
    public int id ;
    @XmlElement(name = "tokenUuid")
    public String uuid ;
    @XmlElement(name = "agentName")
    public String agentName ;
    @XmlElement(name = "agentId")
    public long agentId ;
    @XmlElement(name = "activity")
    public AgentActivity activity ;
    @XmlElement(name = "startDate")
    public Date startDate ;
    @XmlElement(name = "endDate")
    public Date endDate ;


    public TaskModel() {
        // no default constructor for you!
    }

    public int getId() {
        return id;
    }

    public TaskModel setId(int id) {
        this.id = id;
        return this;
    }

    public String getUuid() {
        return uuid;
    }

    public TaskModel setUuid(String uuid) {
        this.uuid = uuid;
        return this;
    }

    public String getAgentName() {
        return agentName;
    }

    public TaskModel setAgentName(String agentName) {
        this.agentName = agentName;
        return this;
    }

    public long getAgentId() {
        return agentId;
    }

    public TaskModel setAgentId(long agentId) {
        this.agentId = agentId;
        return this;
    }

    public AgentActivity getActivity() {
        return activity;
    }

    public TaskModel setActivity(AgentActivity activity) {
        this.activity = activity;
        return this;
    }

    public Date getStartDate() {
        return startDate;
    }

    public TaskModel setStartDate(Date startDate) {
        this.startDate = startDate;
        return this;
    }

    public Date getEndDate() {
        return endDate;
    }

    public TaskModel setEndDate(Date endDate) {
        this.endDate = endDate;
        return this;
    }



    public String asText() {
        return String.format("TASK=%s%n", id);
    }
}
