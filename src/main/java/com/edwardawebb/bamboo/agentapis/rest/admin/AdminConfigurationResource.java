package com.edwardawebb.bamboo.agentapis.rest.admin;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.edwardawebb.bamboo.agentapis.enums.UpgradeStance;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class AdminConfigurationResource {
    @XmlElement
    private UpgradeStance stance;
    @XmlElement
    private int maxactive;
    @XmlElement
    private String targetVersion;
    
    
    public UpgradeStance getStance() {
        return stance;
    }
    public void setStance(UpgradeStance stance) {
        this.stance = stance;
    }
    public int getMaxactive() {
        return maxactive;
    }
    public void setMaxactive(int maxactive) {
        this.maxactive = maxactive;
    }
    public String getTargetVersion() {
        return targetVersion;
    }
    public void setTargetVersion(String targetVersion) {
        this.targetVersion = targetVersion;
    }

}
