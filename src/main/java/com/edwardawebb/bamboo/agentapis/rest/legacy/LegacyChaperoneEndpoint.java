package com.edwardawebb.bamboo.agentapis.rest.legacy;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.sal.api.user.UserManager;
import com.edwardawebb.bamboo.agentapis.ao.AccessTokenService;
import com.edwardawebb.bamboo.agentapis.ao.model.AccessToken;
import com.edwardawebb.bamboo.agentapis.enums.AgentActivity;
import com.edwardawebb.bamboo.agentapis.rest.admin.TaskModel;
import com.edwardawebb.bamboo.agentapis.rest.agents.maintenance.ChaperoneEndpoint;
import com.edwardawebb.bamboo.agentapis.rest.agents.maintenance.UpgradePermissionModel;
import com.edwardawebb.bamboo.agentapis.services.ChaperoneService;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import java.util.UUID;

/**
 * Exposes operations to request and complete maintenance tasks.
 * Chaperone will use rules configured in admin UI to number limit of agents
 * performing maintenance at any time.
 */
@Path("/{id}/maintenance")
@AnonymousAllowed
public class LegacyChaperoneEndpoint {

    private final ChaperoneEndpoint chaperoneEndpoint;

    public LegacyChaperoneEndpoint(ChaperoneEndpoint chaperoneEndpoint){
        this.chaperoneEndpoint = chaperoneEndpoint;
    }


    /**
     * <h4>Request offline maintenance for the agent <em>agentId</em></h4>
     * This service checks against rules in the admin UI.
     * If the agent is 'allowed' by those rules, the agent will be disabled as well.
     * Agents must themselves confirm their status of idle and disabled before upgrading.
     * (Agents may use this response and if needed, addition checks against the /state/ APIs)
     * 
     * <strong>Once upgraded agents should call /finish API.</strong>
     *
     * @param agentId the ID of an online agent
     * @param uuid the UUID of a admin token created in admin UI with 'change' permission
     * @return
     */
    @POST
    public Response upgrade(@PathParam("id") long agentId,@QueryParam("uuid") UUID uuid)
    {
        return chaperoneEndpoint.upgrade(agentId,uuid);
    }

    /**
     * <h4>Mark existing maintenance <em>taskId</em> complete, and re-enable agent</h4>
     * This service will close out an open maintenance record for <em>taskId</em>. If the agent was
     * in an enabled state before maintenance it will also be re-enable.
     *
     * <strong>If agent needs to just re-enable itself, but does not have maintenance, use the /state/ APIs.</strong>
     *
     * @param uuid - the UUID of a token created in Admin UI with 'change' access
     */
    @PUT
    @Path("{taskId}/finish")
    public Response finish(@PathParam("taskId") int taskId,@QueryParam("uuid") UUID uuid)
    {
        return chaperoneEndpoint.finish(taskId,uuid);
    }

    /**
     * <h4>Find open task (if any) for agent <em>agentId</em></h4>
     * This service will return the <em>taskId</em> if there is an open maintenance task for the agent.
     * Otherwise it will return TASK=0
     * @param uuid - the UUID of a token created in Admin UI with 'read' access
     */
    @GET
    @Path("/")
    public Response openTask(@PathParam("id") long agentId,@QueryParam("uuid") UUID uuid){
        return chaperoneEndpoint.openTask(agentId,uuid);
    }


    /**
     * <h4>Shell: Find open task (if any) for agent <em>agentId</em></h4>
     * This service will return the <em>taskId</em> if there is an open maintenance task for the agent.
     * Otherwise it will return TASK=0
     *
     * <strong>The /text API is for a test based response that may be consumed by 'nix shells</strong>
     *
     * @param uuid - the UUID of a token created in Admin UI with 'read' access
     */
    @GET
    @Path("/text")
    public Response openTaskAsText(@PathParam("id") long agentId,@QueryParam("uuid") UUID uuid){
        return chaperoneEndpoint.openTaskAsText(agentId,uuid);
    }
}