package com.edwardawebb.bamboo.agentapis.rest.agents.state;

import javax.xml.bind.annotation.*;
@XmlRootElement(name = "agentState")
@XmlAccessorType(XmlAccessType.FIELD)
public class AgentStateModel {

    private long id;
    private String name;
    private boolean enabled;
    private boolean busy;
    private boolean online;

    public AgentStateModel() {
    }

    /**
     * 
     * @param id
     * @param name
     * @param enabled
     * @param busy
     * @param online
     */
    public AgentStateModel(long id, String name, boolean enabled, boolean busy,boolean online) {
        this.name = name;
        this.id = id;
        this.enabled = enabled;
        this.busy = busy;
        this.online = online;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public boolean isBusy() {
        return busy;
    }

    public void setBusy(boolean busy) {
        this.busy = busy;
    }


    public boolean isOnline() {
        return online;
    }

    public void setOnline(boolean online) {
        this.online = online;
    }
}