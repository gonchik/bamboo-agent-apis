package com.edwardawebb.bamboo.agentapis.rest.agents.capabilities;

import java.util.UUID;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.atlassian.annotations.PublicApi;
import com.atlassian.bamboo.v2.build.agent.capability.ReadOnlyCapabilitySet;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.edwardawebb.bamboo.agentapis.ao.AccessTokenService;
import com.edwardawebb.bamboo.agentapis.ao.model.AccessToken;
import com.edwardawebb.bamboo.agentapis.services.AgentService;

/**
 * A resource of message.
 */
@Path("{id}/capabilities")
@PublicApi
@AnonymousAllowed
@Produces("application/json")
public class Capability {
    
    private AgentService remoteAgentService;
    
    private AccessTokenService accessTokenService;
    
    
    
    

    public Capability(AgentService remoteAgentService, AccessTokenService accessTokenService) {
        this.remoteAgentService = remoteAgentService;
        this.accessTokenService = accessTokenService;
    }

    @GET
    @Path("/")
    public Response lostUserInfo(@PathParam("id") long id,@QueryParam("uuid") UUID uuid)
    {
        if(null == uuid){
            return Response.status(Status.BAD_REQUEST).build();
        }
        AccessToken token = accessTokenService.findTokenByUuid(uuid);
       
        //TODO add new states to token
       if(null != token && token.isAllowedToRead()){
           ReadOnlyCapabilitySet capabilities = remoteAgentService.listCapabilities(id);
           return Response.ok(CapabilityModel.fromCapabilitySet(capabilities)).build();
       }else{
           return Response.status(Status.FORBIDDEN).build();
       }
    }


    /*
     * Methods that report state
     */
    @DELETE
    @Path("/")
    public Response deleteAllCapabilitiesFor(@PathParam("id") long id,@QueryParam("uuid") UUID uuid)
    {
        if(null == uuid){
            return Response.status(Status.BAD_REQUEST).build();
        }
        AccessToken token = accessTokenService.findTokenByUuid(uuid);
       
        //TODO add new states to token
       if(null != token && token.isAllowedToChange()){
           remoteAgentService.deleteAllCapabilities(id);
           return Response.ok().build();
       }else{
           return Response.status(Status.FORBIDDEN).build();
       }
        
    }
}