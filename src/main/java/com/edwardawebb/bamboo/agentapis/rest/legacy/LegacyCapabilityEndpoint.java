package com.edwardawebb.bamboo.agentapis.rest.legacy;

import com.atlassian.annotations.PublicApi;
import com.atlassian.bamboo.v2.build.agent.capability.ReadOnlyCapabilitySet;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.edwardawebb.bamboo.agentapis.ao.AccessTokenService;
import com.edwardawebb.bamboo.agentapis.ao.model.AccessToken;
import com.edwardawebb.bamboo.agentapis.rest.agents.capabilities.Capability;
import com.edwardawebb.bamboo.agentapis.rest.agents.capabilities.CapabilityModel;
import com.edwardawebb.bamboo.agentapis.services.AgentService;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import java.util.UUID;

/**
 * A resource of message.
 */
@Path("{id}/capabilities")
@PublicApi
@AnonymousAllowed
@Produces("application/json")
public class LegacyCapabilityEndpoint {

    private final Capability capabilityEndpoint;



    public LegacyCapabilityEndpoint(Capability capabilityEndpoint) {
        this.capabilityEndpoint = capabilityEndpoint;
    }

    @GET
    @Path("/")
    public Response lostUserInfo(@PathParam("id") long id,@QueryParam("uuid") UUID uuid)
    {
        return capabilityEndpoint.lostUserInfo(id,uuid);
    }


    /*
     * Methods that report state
     */
    @DELETE
    @Path("/")
    public Response deleteAllCapabilitiesFor(@PathParam("id") long id,@QueryParam("uuid") UUID uuid)
    {
        return capabilityEndpoint.deleteAllCapabilitiesFor(id,uuid);
    }
}