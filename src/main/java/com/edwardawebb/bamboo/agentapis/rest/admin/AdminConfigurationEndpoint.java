package com.edwardawebb.bamboo.agentapis.rest.admin;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.atlassian.annotations.PublicApi;
import com.edwardawebb.bamboo.agentapis.services.ChaperoneService;
import com.edwardawebb.bamboo.agentapis.services.ConfigurationService;
import org.tuckey.web.filters.urlrewrite.utils.StringUtils;

import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.sal.api.user.UserManager;
import com.edwardawebb.bamboo.agentapis.enums.UpgradeStance;

@Path("/admin-config")
@PublicApi
public class AdminConfigurationEndpoint {

    private final UserManager userManager;
    private final ConfigurationService configurationService;
    private final TransactionTemplate transactionTemplate;

    public AdminConfigurationEndpoint(UserManager userManager, ConfigurationService configurationService,
            TransactionTemplate transactionTemplate) {
        this.userManager = userManager;
        this.configurationService = configurationService;
        this.transactionTemplate = transactionTemplate;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response get(@Context HttpServletRequest request) {
        String username = userManager.getRemoteUsername(request);
        if (username == null || !userManager.isSystemAdmin(username)) {
            return Response.status(Status.UNAUTHORIZED).build();
        }

        return Response.ok(transactionTemplate.execute(new TransactionCallback() {
            public Object doInTransaction() {
                return configurationService.getActiveConfig();
            }
        })).build();
    }
    
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response put(final AdminConfigurationResource config, @Context HttpServletRequest request)
    {
      String username = userManager.getRemoteUsername(request);
      if (username == null || !userManager.isSystemAdmin(username))
      {
        return Response.status(Status.UNAUTHORIZED).build();
      }

      transactionTemplate.execute(new TransactionCallback()
      {
        public Object doInTransaction()
        {
          configurationService.updateConfiguration(config);
          return null;
        }
      });
      return Response.noContent().build();
    }

}
