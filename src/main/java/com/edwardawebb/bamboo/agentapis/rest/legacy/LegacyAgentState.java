package com.edwardawebb.bamboo.agentapis.rest.legacy;

import com.atlassian.annotations.PublicApi;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.edwardawebb.bamboo.agentapis.ao.AccessTokenService;
import com.edwardawebb.bamboo.agentapis.ao.model.AccessToken;
import com.edwardawebb.bamboo.agentapis.rest.agents.state.AgentState;
import com.edwardawebb.bamboo.agentapis.rest.agents.state.AgentStateModel;
import com.edwardawebb.bamboo.agentapis.services.AgentService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import java.util.UUID;

/**
 * A resource of message.
 */
@PublicApi
@Path("/")
public class LegacyAgentState {

   private final AgentState agentState;


    public LegacyAgentState(AgentState agentState) {
        this.agentState = agentState;
    }

    /**
     * <h4>List Agents</h4>
     * This service lists agent IDs and:
     * <ul>
     *     <li>ENABLED - is the agent enabled? (false==disabled)</li>
     *     <li>BUSY - is the agent currently running a job?</li>
     *     <li>ONLINE - is the agent actively connected? (may still be disabled)</li>
     * </ul>
     *
     * @param uuid - the UUID of a token created in Admin UI with 'read' access
     */
    @GET
    @AnonymousAllowed
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllAgentsState(@QueryParam("uuid") UUID uuid)
    {
        return agentState.getAllAgentsState(uuid);
    }


    /**
     * <h4>Shell: Return State information for agent <em>agentId</em></h4>
     * This service will return the following statuses:
     * <ul>
     *     <li>ENABLED - is the agent enabled? (false==disabled)</li>
     *     <li>BUSY - is the agent currently running a job?</li>
     *     <li>ONLINE - is the agent actively connected? (may still be disabled)</li>
     * </ul>
     *
     * <strong>The /text API is for a test based response that may be consumed by 'nix shells</strong>
     *
     * @param uuid - the UUID of a token created in Admin UI with 'read' access
     */
    @GET
    @AnonymousAllowed
    @Path("/{id}/state/text")
    public Response getBusyShorthandDeprecated(@PathParam("id") long id,@QueryParam("uuid") UUID uuid)
    {
        return agentState.getBusyShorthandDeprecated(id, uuid);
    }


    /**
     * <h4>Return State information for agent <em>agentId</em></h4>
     * This service will return the following statuses:
     * <ul>
     *     <li>ENABLED - is the agent enabled? (false==disabled)</li>
     *     <li>BUSY - is the agent currently running a job?</li>
     *     <li>ONLINE - is the agent actively connected? (may still be disabled)</li>
     * </ul>
     * @param uuid - the UUID of a token created in Admin UI with 'read' access
     */
    @GET
    @AnonymousAllowed
    @Path("/{id}/state")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCurrentState(@PathParam("id") long id,@QueryParam("uuid") UUID uuid)
    {
       return agentState.getCurrentState(id,uuid);
    }




    /**
     * <h4>Enable the agent <em>agentId</em></h4>
     * This service will mark the agent enabled (whether or not its online)
     *
     * @param uuid - the UUID of a token created in Admin UI with 'change' access
     */
    @POST
    @AnonymousAllowed
    @Path("/{id}/state/enable")
    @Produces(MediaType.APPLICATION_JSON)
    public Response disableAgentDeprecated(@PathParam("id") long id,@QueryParam("uuid") UUID uuid)
    {
       return agentState.disableAgentDeprecated(id,uuid);
    }

    /**
     * <h4>Disable the agent <em>agentId</em></h4>
     * This service will mark the agent disabled (whether or not its online)
     *
     * @param uuid - the UUID of a token created in Admin UI with 'change' access
     */
    @POST
    @AnonymousAllowed
    @Path("/{id}/state/disable")
    @Produces(MediaType.APPLICATION_JSON)
    public Response enableAgentDeprecated(@PathParam("id") long id,@QueryParam("uuid") UUID uuid)
    {
        return agentState.enableAgentDeprecated(id,uuid);
    }
    
}