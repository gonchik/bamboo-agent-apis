package com.edwardawebb.bamboo.agentapis.rest.agents.maintenance;

import com.edwardawebb.bamboo.agentapis.enums.PermissionEnum;
import com.edwardawebb.bamboo.agentapis.rest.agents.state.AgentStateModel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * This represents the response payload returned by mother when children ask for
 * permission. Response codes are unique to all circumstances. isAllowed will
 * always be true or false.
 * 
 * TODO: This class is overworked
 * 
 */
@XmlRootElement(name = "permissionResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class VersionModel {

    @XmlElement(name = "targetVersion")
    public String targetVersion = "LATEST";


    private VersionModel() {
        // no default constructor for you!
    }

    public VersionModel( String targetVersion) {
        this.targetVersion = targetVersion;
    }


    public String asText() {
        return String.format("PVERSION=%s%n",  targetVersion);
    }

}
