package com.edwardawebb.bamboo.agentapis.rest.agents.maintenance;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.edwardawebb.bamboo.agentapis.enums.PermissionEnum;
import com.edwardawebb.bamboo.agentapis.rest.agents.state.AgentStateModel;

/**
 * This represents the response payload returned by mother when children ask for
 * permission. Response codes are unique to all circumstances. isAllowed will
 * always be true or false.
 * 
 * TODO: This class is overworked
 * 
 */
@XmlRootElement(name = "permissionResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class UpgradePermissionModel {

    @XmlElement(name = "permissionCode")
    public PermissionEnum permissionCode;
    @XmlElement(name = "targetVersion")
    public String targetVersion = "LATEST";
    @XmlElement(name = "message")
    public String message;
    @XmlElement(name = "agentState")
    public AgentStateModel agentState;
    @XmlElement(name = "taskId")
    public int taskId;
   

    private UpgradePermissionModel() {
        // no default constructor for you!
    }

    public UpgradePermissionModel(PermissionEnum permissionEnum, String message, AgentStateModel agentState) {
        this.permissionCode = permissionEnum;
        this.message = message;
        this.agentState = agentState;
    }

    public UpgradePermissionModel(PermissionEnum permissionEnum, String message, AgentStateModel agentState, String targetVersion) {
        this.permissionCode = permissionEnum;
        this.message = message;
        this.targetVersion = targetVersion;
        this.agentState = agentState;
    }

    public UpgradePermissionModel(PermissionEnum permissionEnum, String message, AgentStateModel agentState, String targetVersion, int taskId) {
        this(permissionEnum,message,agentState,targetVersion);
        this.taskId = taskId;
    }

    public String asText() {
        return String.format("PCODE=%s%nPMESSAGE=\"%s\"%nPVERSION=%s%nENABLED=%s%nBUSY=%s%nTASK=%s%n", permissionCode, message, targetVersion,agentState.isEnabled(),agentState.isBusy(),taskId);
    }

}
