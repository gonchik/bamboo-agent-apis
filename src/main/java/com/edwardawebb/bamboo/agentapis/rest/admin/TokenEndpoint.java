package com.edwardawebb.bamboo.agentapis.rest.admin;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.atlassian.annotations.PublicApi;
import com.atlassian.sal.api.user.UserManager;
import com.edwardawebb.bamboo.agentapis.ao.AccessTokenService;
import com.edwardawebb.bamboo.agentapis.ao.model.AccessToken;

/**
 * This class manages (CRUDs) the tokens that may be used in calls to {@link AgentState} REST endpoint.
 */
@Path("/tokens")
@PublicApi
@Produces("application/json")
public class TokenEndpoint  {
    
    private final AccessTokenService accessTokenService;
    private final UserManager userManager;
   
    

    public TokenEndpoint(AccessTokenService accessTokenService,UserManager userManager) {
        this.accessTokenService = accessTokenService;
        this.userManager=userManager;
    }

   
    /*
     * Methods that report state
     */
    @GET
    @Path("/")
    public Response listAllTokens()
    {
        if(!isAdmin()){
            return Response.status(Status.FORBIDDEN).build();
        }
        AccessToken[] tokens = accessTokenService.findAll();
        List<TokenResource> resources = new ArrayList<TokenResource>();
        for (int i = 0; i < tokens.length; i++) {
            resources.add(TokenResource.from(tokens[i]));
        }
        return Response.ok(resources).build();
       
    }
    

   
    @GET
    @Path("/{id}")
    public Response getTokenInfo(@PathParam("id") UUID uuid)
    {
        if(!isAdmin()){
            return Response.status(Status.FORBIDDEN).build();
        }
       AccessToken token = accessTokenService.findTokenByUuid(uuid);
       if(null != token){
           return Response.ok(TokenResource.from(token)).build();
       }else{
           return Response.status(Status.NOT_FOUND).build();
       }
    }
    
   

    
    
    
    /*
     * Methods that alter state
     */
    @POST
    @Path("/")
    @Consumes({ MediaType.APPLICATION_JSON })
    public Response generateNewToken( TokenResource resource)
    {  
        if(!isAdmin()){
            return Response.status(Status.FORBIDDEN).build();
        }    
       AccessToken token = accessTokenService.createNewToken(resource);
       return Response.ok(TokenResource.from(token)).build();
    }
    
    @PUT
    @Path("/{id}")
    @Consumes({ MediaType.APPLICATION_JSON })
    public Response updateTokenInfo(@PathParam("id") UUID uuid, TokenResource resource)
    {
        if(!isAdmin()){
            return Response.status(Status.FORBIDDEN).build();
        }
       AccessToken token = accessTokenService.updateToken(resource);
       return Response.ok(TokenResource.from(token)).build();
    }
    
    @DELETE
    @Path("/{id}")
    public Response updateTokenInfo(@PathParam("id") UUID uuid)
    {
        if(!isAdmin()){
            return Response.status(Status.FORBIDDEN).build();
        }
       
        accessTokenService.purgeToken(uuid);
       return Response.ok().build();
    }
    
    
    /*
     * private methods
     * 
     */
    

    private boolean isAdmin() {
        String username = userManager.getRemoteUsername();
        if ( userManager.isAdmin(username) || userManager.isSystemAdmin(username) ){
            return true;
        }else{
            return false;
        }    
    }


}