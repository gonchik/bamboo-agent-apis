/**
 * 
 */
package com.edwardawebb.bamboo.agentapis.ao;

import java.util.UUID;

import com.edwardawebb.bamboo.agentapis.ao.model.AccessToken;
import com.edwardawebb.bamboo.agentapis.rest.admin.TokenResource;

/**
 * @author n0158588
 *
 */
public interface AccessTokenService {
    AccessToken createNewToken(String name,boolean canRead, boolean canChange);
    AccessToken updateToken(int id, UUID uuid,String name,boolean canRead, boolean canChange );
    void purgeToken(UUID uuid);
    
    AccessToken findTokenByUuid(UUID uuid);
    AccessToken updateToken(TokenResource resource);
    AccessToken[] findAll();
    AccessToken createNewToken(TokenResource resource);
    
}
