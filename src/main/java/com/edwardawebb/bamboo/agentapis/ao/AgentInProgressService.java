package com.edwardawebb.bamboo.agentapis.ao;

import java.util.List;
import java.util.UUID;

import com.atlassian.bamboo.v2.build.agent.BuildAgent;
import com.edwardawebb.bamboo.agentapis.ao.model.AgentInProgress;
import com.edwardawebb.bamboo.agentapis.enums.AgentActivity;
import com.edwardawebb.bamboo.agentapis.rest.agents.state.AgentStateModel;

public interface AgentInProgressService {
    
    AgentInProgress markAgentInProgress(BuildAgent agent, AgentActivity activity, UUID uuid, AgentStateModel agentState);

    List<AgentInProgress> existingActivityFor(long agentId);

    List<AgentInProgress> existingActivityForAllAgents();

    AgentInProgress markTaskComplete(int taskId);

}
