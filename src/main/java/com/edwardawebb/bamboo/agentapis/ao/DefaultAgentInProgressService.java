package com.edwardawebb.bamboo.agentapis.ao;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import net.java.ao.DBParam;
import net.java.ao.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.bamboo.v2.build.agent.BuildAgent;
import com.edwardawebb.bamboo.agentapis.ao.model.AgentInProgress;
import com.edwardawebb.bamboo.agentapis.enums.AgentActivity;
import com.edwardawebb.bamboo.agentapis.rest.agents.maintenance.AlreadyInProgressException;
import com.edwardawebb.bamboo.agentapis.rest.agents.state.AgentStateModel;
import com.edwardawebb.bamboo.agentapis.services.audit.AgentApiAuditService;

public class DefaultAgentInProgressService implements AgentInProgressService{
    private final ActiveObjects ao;
    private AgentApiAuditService auditService;
    private static final Logger log = LoggerFactory
            .getLogger(DefaultAgentInProgressService.class);

    public DefaultAgentInProgressService(ActiveObjects activeObjects, AgentApiAuditService agentApiAuditService) {
        this.ao = activeObjects;
        this.auditService = agentApiAuditService;
    }
    
    
    /**
     * No logic, you could tell me to take your 101th
     *  agent in progress with 100 others busy too and I'll mark it so in my records
     */
    @Override
    public AgentInProgress markAgentInProgress(BuildAgent agent, AgentActivity activity, UUID uuid, AgentStateModel agentState) {
       List<AgentInProgress> existing = existingActivityFor(agent.getId());
       if (existing.size()>0){
           throw new AlreadyInProgressException("Agent already marked in progress for acitvities, will not insert new row.",existing.get(0).getID());
       }
       log.debug("Agent is " + agent.isEnabled() + " and will be left in that state when maintenance completes");
        AgentInProgress record = ao.create(AgentInProgress.class,
                new DBParam("AID",agent.getId()),
                new DBParam("ACTION",activity),
                new DBParam("START_DATE",new Date()),
                new DBParam("NAME",agentState.getName()),
                new DBParam("UUID",uuid.toString()),
                new DBParam("ACTIVE",true),
                new DBParam("AGNTSTAT",agent.isEnabled()));
        auditService.startAcvityEvent(record);
        
        
        return record;
    }


    @Override
    public List<AgentInProgress> existingActivityFor(long agentId) {
        AgentInProgress[] result = ao.find(AgentInProgress.class, Query.select().where("AID = ? and ACTIVE = ?",agentId,Boolean.TRUE));
        return Arrays.asList(result);
    }


    @Override
    public List<AgentInProgress> existingActivityForAllAgents() {
        AgentInProgress[] result = ao.find(AgentInProgress.class, Query.select().where("ACTIVE = ?", true));
        for (AgentInProgress agentInProgress : result) {
            log.warn("Agent: {}",agentInProgress.toString());
        }
        return Arrays.asList(result);
        
    }


    @Override
    public AgentInProgress markTaskComplete(int taskId) {
        AgentInProgress task = ao.get(AgentInProgress.class, taskId);
        if (null == task || 0 == task.getAgentId() ){
            log.warn("Attempt to complete invalid Task ID: {}",taskId);
            throw new RuntimeException("Attempt to complete invalid Task ID: " +taskId);
        }
       task.setInProgress(false);
       task.setEndDate(new Date());
       task.save();
       return task;
    }

}
