package com.edwardawebb.bamboo.agentapis.enums;

public enum AgentActivity {

    UPGRADE("An agent requested to be take offline while it updates local binaries and configuration"),
    MAINTENANCE("An agent is performing maintenance such as cleaning filesystem.");






    private String description;
    
    
    AgentActivity(String description){
        this.description = description;
    }

}
