package com.edwardawebb.bamboo.agentapis.tasks;


import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.deployments.execution.DeploymentTaskContext;
import com.atlassian.bamboo.deployments.execution.DeploymentTaskType;
import com.atlassian.bamboo.deployments.results.DeploymentResult;
import com.atlassian.bamboo.task.TaskContext;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;
import com.atlassian.bamboo.task.TaskType;
import com.atlassian.bamboo.v2.build.CurrentResult;
import com.atlassian.bamboo.variable.CustomVariableContext;
import com.edwardawebb.bamboo.agentapis.enums.UpgradeStance;
import com.edwardawebb.bamboo.agentapis.rest.admin.AdminConfigurationResource;
import com.edwardawebb.bamboo.agentapis.services.ConfigurationService;

import java.util.Map;
import java.util.Set;

public class UpdateRulesTask implements DeploymentTaskType
{
    private final ConfigurationService configurationService;
    private final CustomVariableContext customVariableContext;

    public UpdateRulesTask(ConfigurationService configurationService,CustomVariableContext customVariableContext) {
        this.configurationService = configurationService;
        this.customVariableContext = customVariableContext;
    }

    @Override
    public TaskResult execute(final DeploymentTaskContext taskContext) throws TaskException
    {
        String versionKey = "deploy.version";
        final BuildLogger buildLogger = taskContext.getBuildLogger();
        Map<String, String> variables = this.customVariableContext.getVariables(taskContext.getCommonContext());

        String version = variables.get(versionKey);
        buildLogger.addBuildLogEntry("Setting agent Upgrade Stance to TARGET with release version of " + version + " (from variable " + versionKey + ")");
        try {
            AdminConfigurationResource config = configurationService.getActiveConfig();
            config.setTargetVersion(version);
            config.setStance(UpgradeStance.TARGET);
            configurationService.updateConfiguration(config);
            buildLogger.addBuildLogEntry("Version " + version + " is now the target for all polling agents!");

            return TaskResultBuilder.newBuilder(taskContext).success().build();
        }catch (Throwable e){
            buildLogger.addErrorLogEntry("Error updating config service with version",e);
            return TaskResultBuilder.newBuilder(taskContext).failedWithError().build();
        }
    }
}