package com.edwardawebb.bamboo.agentapis.services;

import com.edwardawebb.bamboo.agentapis.rest.admin.AdminConfigurationResource;

/**
 * Created by n0158588 on 9/16/16.
 */
public interface ConfigurationService {
    String SETTINGS_KEY_STANCE = ".stance";
    String SETTINGS_KEY_VERSION = ".targetVersion";
    String SETTINGS_KEY_MAXACTIVE = ".maxActive";

    AdminConfigurationResource getActiveConfig();

    void updateConfiguration(AdminConfigurationResource config);
}
