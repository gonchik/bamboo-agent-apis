package com.edwardawebb.bamboo.agentapis.services;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.log4j.Logger;

import com.atlassian.bamboo.buildqueue.ElasticAgentDefinition;
import com.atlassian.bamboo.buildqueue.LocalAgentDefinition;
import com.atlassian.bamboo.buildqueue.PipelineDefinition;
import com.atlassian.bamboo.buildqueue.PipelineDefinitionVisitor;
import com.atlassian.bamboo.buildqueue.RemoteAgentDefinition;
import com.atlassian.bamboo.buildqueue.manager.AgentManager;
import com.atlassian.bamboo.v2.build.agent.BuildAgent;
import com.atlassian.bamboo.v2.build.agent.capability.Capability;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilitySet;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilitySetImpl;
import com.atlassian.bamboo.v2.build.agent.capability.ReadOnlyCapabilitySet;
import com.edwardawebb.bamboo.agentapis.AgentNotValidException;
import com.edwardawebb.bamboo.agentapis.rest.agents.state.AgentStateModel;
import sun.management.resources.agent;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class DefaultAgentService implements AgentService {

    private static final Logger LOG = Logger.getLogger(DefaultAgentService.class);
    private final AgentManager agentManager;
    
    public DefaultAgentService(AgentManager agentManager) {
        this.agentManager = agentManager;
    }

    public AgentManager getAgentManager(){
        return agentManager;
    }
 
    @Override
    public AgentStateModel getStateFor(long key) {
        PipelineDefinition defintion = agentManager.getAgentDefinition(key);
        BuildAgent agent = agentManager.getAgent(key);
        boolean isBusy = agent.isBusy();
        boolean isOnline = agent.isActive();
        return new AgentStateModel(key, agent.getName(),agent.isEnabled(),isBusy,isOnline);
    }

    @Override
    public List<AgentStateModel> getAgents() {
        List<BuildAgent> agents = this.agentManager.getAllAgents();
        List<AgentStateModel> agentsList = new ArrayList<AgentStateModel>();
        for(BuildAgent agent : agents){
            AgentStateModel stateModel = new AgentStateModel(agent.getId(), agent.getName(), agent.isEnabled(), agent.isBusy(),agent.isActive());
            agentsList.add(stateModel);
        }
        return agentsList;
    }

    @Override
    public AgentStateModel disable(long key) {
        PipelineDefinition agent = agentManager.getAgentDefinition(key);
        if(null == agent){
            throw new AgentNotValidException("Could not load an agent with ID "+ key);
        }
        agent.setEnabled(false);
        agentManager.saveAnyPipeline(agent);
        return getStateFor(key);
    }
    
    @Override
    public AgentStateModel enable(long key) {
        PipelineDefinition agent = agentManager.getAgentDefinition(key);
        if(null == agent){
            throw new AgentNotValidException("Could not load an agent with ID "+ key);
        }
        agent.setEnabled(true);        
        agentManager.saveAnyPipeline(agent);
        return getStateFor(key);
    }

    @Override
    public ReadOnlyCapabilitySet listCapabilities(long agentId){    
        PipelineDefinition agent = agentManager.getAgentDefinition(agentId);
        final CapabilitySet results = new CapabilitySetImpl();
        agent.accept(new PipelineDefinitionVisitor()
        {
            @Override
            public void visitElastic(ElasticAgentDefinition pipelineDefinition)
            {
                throw new IllegalArgumentException("You must manually change your elastic instance config.");
            }

            @Override
            public void visitLocal(LocalAgentDefinition pipelineDefinition)
            {
                CapabilitySet capabilitySet = pipelineDefinition.getCapabilitySet();
                if (null != capabilitySet)
                {
                    LOG.debug("Local agent has: " + capabilitySet.getCapabilities().size());
                    results.setCapabilities(capabilitySet.getCapabilities());
                }
            }

            @Override
            public void visitRemote(RemoteAgentDefinition pipelineDefinition)
            {
                CapabilitySet capabilitySet = pipelineDefinition.getCapabilitySet();
                if (null != capabilitySet)
                {
                    LOG.debug("Remote agent has: " + capabilitySet.getCapabilities().size());
                    results.setCapabilities(capabilitySet.getCapabilities());
                }
            }
        });
        LOG.debug("Returning " + results.getCapabilities().size() + " capabilities");
        return results;
    }
    
    
    @Override
    public void deleteAllCapabilities(final long agentId){   
        final BuildAgent buildAgent = agentManager.getAgent(agentId);
        PipelineDefinition agent = buildAgent.getDefinition();

        agent.accept(new PipelineDefinitionVisitor()
        {
            @Override
            public void visitElastic(ElasticAgentDefinition pipelineDefinition)
            {
                throw new IllegalArgumentException("You must manually change your elastic instance config.");
            }
            @Override
            public void visitLocal(LocalAgentDefinition pipelineDefinition)
            {
                LOG.info("Deleting all agent-specific capabilities on local agent " + agentId);
                CapabilitySet capabilitySet = pipelineDefinition.getCapabilitySet();
                if (capabilitySet != null)
                {
                    for (Capability capability : capabilitySet.getCapabilities()) {
                        capabilitySet.removeCapability(capability.getKey());
                    }
                }
                agentManager.savePipeline(pipelineDefinition);
            }

            @Override
            public void visitRemote(RemoteAgentDefinition pipelineDefinition)
            {
                LOG.info("Deleting all agent-specific capabilities on remote agent " + agentId);
                CapabilitySet capabilitySet = pipelineDefinition.getCapabilitySet();
                if (capabilitySet != null)
                {
                    Set<Capability> set = capabilitySet.getCapabilities();
                    if (set != null)
                    {
                        set.clear();
                    }
                }
                agentManager.savePipeline(pipelineDefinition);
            }
        });
        
    }


}
