package com.edwardawebb.bamboo.agentapis.services;


import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.edwardawebb.bamboo.agentapis.enums.UpgradeStance;
import com.edwardawebb.bamboo.agentapis.rest.admin.AdminConfigurationResource;
import org.tuckey.web.filters.urlrewrite.utils.StringUtils;

public class DefaultConfigurationService implements ConfigurationService {

    private final PluginSettingsFactory pluginSettingsFactory;

    public DefaultConfigurationService(PluginSettingsFactory pluginSettingsFactory) {
        this.pluginSettingsFactory = pluginSettingsFactory;
    }

    @Override
    public AdminConfigurationResource getActiveConfig(){
        PluginSettings settings = pluginSettingsFactory.createGlobalSettings();
        AdminConfigurationResource config = new AdminConfigurationResource();
        try{
            config.setStance( UpgradeStance.valueOf((String) settings.get(AdminConfigurationResource.class.getName() + SETTINGS_KEY_STANCE)));
        }catch(NullPointerException npe){
            //first time, select list will default.
            config.setStance( UpgradeStance.STABLE);
        }
        String targetVersion = (String) settings.get(AdminConfigurationResource.class.getName() + SETTINGS_KEY_VERSION);
        if (targetVersion != null) {
            config.setTargetVersion(targetVersion);
        }

        String maxActive = (String) settings.get(AdminConfigurationResource.class.getName() + SETTINGS_KEY_MAXACTIVE);
        if (maxActive != null) {
            config.setMaxactive(Integer.parseInt(maxActive));
        }
        return config;
    }

    @Override
    public void updateConfiguration(AdminConfigurationResource config) {
        PluginSettings pluginSettings = pluginSettingsFactory.createGlobalSettings();
        pluginSettings.put(AdminConfigurationResource.class.getName() + SETTINGS_KEY_STANCE, config.getStance().name());
        if(config.getStance().equals(UpgradeStance.LATEST)){
            pluginSettings.put(AdminConfigurationResource.class.getName()  + SETTINGS_KEY_VERSION,UpgradeStance.LATEST.name());
        }else{
            String version = config.getTargetVersion();
            if(StringUtils.isBlank(version)){
                version="LATEST";
            }
            pluginSettings.put(AdminConfigurationResource.class.getName()  + SETTINGS_KEY_VERSION,version);
        }
        pluginSettings.put(AdminConfigurationResource.class.getName()  + SETTINGS_KEY_MAXACTIVE, Integer.toString(config.getMaxactive()));
    }
}
